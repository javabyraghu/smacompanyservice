package in.nareshit.raghu.service;

import java.util.List;

import in.nareshit.raghu.entity.Company;

public interface ICompanyService {

	Long createCompany(Company cob);
	void updateCompany(Company cob);
	Company getOneCompany(Long id);
	List<Company> getAllCompanies();
}
